import { Request, Response, Router } from "express";
import { ICreateUserUseCase } from "~/domain/interfaces/use-cases/create-user.interface";
import { IGetAllUsersUseCase } from "~/domain/interfaces/use-cases/get-users.interface";

export default function UsersRouter(
    getAllUsersUseCase: IGetAllUsersUseCase,
    createUserUseCase: ICreateUserUseCase
) {
    const router = Router();

    router.get("/", async (req: Request, res: Response) => {
        try {
            const users = await getAllUsersUseCase.execute();
            res.status(200).send(users)
        } catch (error) {
            console.error(error);
            res.status(500).send({ success: false, message: "Error fetching data" })
        }
    });

    router.post("/", async (req: Request, res: Response) => {
        try {
            const { email, password } = req.body;
            await createUserUseCase.execute({ email, password });
            res.send({ success: true })
        } catch (error) {
            res.status(500).send({ success: false, message: "Error saving data" })
        }
    })

    return router;
}