import { IDatabaseWrapper } from "../db-wrapper.interface";
import { UserModel } from "./user/user-model.datasource";

export async function getMongoDC() {
    const userDatabase: IDatabaseWrapper = {
        find: (query) => UserModel.find(),
        insertOne: (doc) => UserModel.create()
    }
}