import { model, Schema } from "mongoose";

const UserScema = new Schema({
    name: String
});

export const UserModel = model('Test', UserScema);
