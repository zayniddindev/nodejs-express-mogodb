import { ICreateUser, IUser } from "~/domain/entities/user.entity";
import { IUserDataSource } from "../../../interfaces/user-ds.interface";
import { IDatabaseWrapper } from "../../db-wrapper.interface";

export class MongoDBUsersDataSource implements IUserDataSource {
    private database: IDatabaseWrapper;
    constructor(database: IDatabaseWrapper) {
        this.database = database;
    }

    async create(user: ICreateUser): Promise<boolean> {
        const result = await this.database.insertOne(user);
        return result !== null
    }

    async getAll(): Promise<IUser[]> {
        const result = await this.database.find({});
        return result.map(item => ({
            id: item._id.toString(),
            email: item.email,
            password: item.password,
            first_name: item.first_name,
            last_name: item.last_name,
            username: item.username
        }))
    }
}