import { ICreateUser, IUser } from "~/domain/entities/user.entity";

export interface IUserDataSource {
    create(user: ICreateUser): Promise<boolean>;
    getAll(): Promise<IUser[]>;
}