export interface IUser {
    id: string;
    email: string;
    first_name?: string;
    last_name?: string;
    username?: string;
    password: string;
}

export interface ICreateUser {
    email: string;
    password: string;
}