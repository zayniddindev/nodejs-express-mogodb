import { IUser } from "~/domain/entities/user.entity";

export interface IGetAllUsersUseCase {
    execute(): Promise<IUser[]>;
}