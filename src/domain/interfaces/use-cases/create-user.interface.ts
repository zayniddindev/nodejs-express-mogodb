import { ICreateUser } from "~/domain/entities/user.entity";

export interface ICreateUserUseCase {
    execute(user: ICreateUser): Promise<boolean>;
}