import { ICreateUser, IUser } from "~/domain/entities/user.entity";

export interface IUserRepository {
    create(user: ICreateUser): Promise<boolean>;
    getAll(): Promise<IUser[]>;
}