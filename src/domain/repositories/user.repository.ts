import { IUserDataSource } from "~/infra/data/interfaces/user-ds.interface";
import { ICreateUser, IUser } from "../entities/user.entity";
import { IUserRepository } from "../interfaces/repositories/user-repository.interface";

export class UserRepository implements IUserRepository {
    dataSource: IUserDataSource;
    constructor(userDataSource: IUserDataSource) {
        this.dataSource = userDataSource;
    }

    async create(user: ICreateUser): Promise<boolean> {
        const result = await this.dataSource.create(user);
        return result
    }

    async getAll(): Promise<IUser[]> {
        return this.dataSource.getAll()
    }
}